package com.example.weatherapp.repository

import com.example.weatherapp.retrofit.WeatherInterface
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WeatherRepoTest {
    private lateinit var weatherRepo: WeatherRepo

    @Mock
    private lateinit var weatherInterface: WeatherInterface

    @Before
    fun setUp() {
        weatherRepo = WeatherRepo()
    }

    @Test
    fun getErrorData() {

    }

    @Test
    fun getInstance() {
    }

    @Test
    fun getWeatherData() {
    }

}