package com.example.weatherapp.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

data class Weather(
    var timezone: String,
    var currently: CurrentlyWeather?
)
@Parcelize
data class CurrentlyWeather(
    var time: Float,
    var summary: String,
    var icon: String,
    var nearestStormDistance: Float,
    var nearestStormBearing: Float,
    @SerializedName("precipIntensity")
    var intensity: Float,
    @SerializedName("precipProbability")
    var probability: Float,
    var temperature: Float,
    var apparentTemperature: Float,
    var dewPoint: Float,
    var humidity: Float,
    var pressure: Float,
    var windSpeed: Float,
    var windGust: Float,
    var windBearing: Float,
    var cloudCover: Float,
    var uvIndex: Float,
    var visibility: Float,
    var ozone: Float
) : Parcelable