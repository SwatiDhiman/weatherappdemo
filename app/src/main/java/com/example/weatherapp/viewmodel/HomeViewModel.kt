package com.example.weatherapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherapp.model.Weather
import com.example.weatherapp.repository.WeatherRepo

class HomeViewModel : ViewModel() {
    private var data: MutableLiveData<Weather>? = null
    private var error: MutableLiveData<String>? = null
    private var weatherRepo: WeatherRepo? = null

    fun init() {
        if (data != null) {
            return
        }
        weatherRepo = WeatherRepo().getInstance()
    }

    fun getData(key: String, lat: Double, long: Double): LiveData<Weather>? {
        data = weatherRepo?.getWeatherData(key, lat, long)
        return data
    }

    fun getError(): MutableLiveData<String>? {
        error = weatherRepo?.getError()
        return error
    }
}