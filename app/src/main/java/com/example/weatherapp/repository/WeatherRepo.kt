package com.example.weatherapp.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.weatherapp.model.Weather
import com.example.weatherapp.retrofit.WeatherInterface
import com.example.weatherapp.retrofit.WeatherService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class WeatherRepo {
    private var weatherRepo: WeatherRepo? = null

    val errorData = MutableLiveData<String>()

    fun getInstance(): WeatherRepo? {
        if (weatherRepo == null) {
            weatherRepo = WeatherRepo()
        }
        return weatherRepo
    }

    private val weatherInterface = WeatherService().provideApi()

    /**
     * load repositories
     */
    fun getWeatherData(key: String, lat: Double, long: Double): MutableLiveData<Weather> {
        val weatherData = MutableLiveData<Weather>()
        weatherInterface.getWeather(key, lat, long).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribeWith(object : DisposableObserver<Weather>() {
                override fun onComplete() {

                }

                override fun onNext(value: Weather?) {
                    weatherData.value = value
                }

                override fun onError(e: Throwable?) {
                    errorData.value = e?.message
                    weatherData.value = null
                }

            })
        return weatherData
    }

    fun getError(): MutableLiveData<String>? {
        return errorData
    }
}