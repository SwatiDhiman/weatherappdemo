package com.example.weatherapp.retrofit

import com.example.weatherapp.model.Weather
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.*

interface WeatherInterface {
    @GET("{key}/{latitude},{longitude}")
    fun getWeather(
        @Path("key") key: String,
        @Path("latitude") latitude: Double,
        @Path("longitude") longitude: Double
    ): Observable<Weather>
}