package com.example.weatherapp.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.weatherapp.R
import com.example.weatherapp.model.CurrentlyWeather
import com.example.weatherapp.model.Weather
import kotlinx.android.synthetic.main.fragment_details.*
import org.w3c.dom.Text

/**
 * A simple [Fragment] subclass.
 */
class DetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        val textViewHumidity = view.findViewById<TextView>(R.id.textViewHumidity)
        val textViewUvIndex = view.findViewById<TextView>(R.id.textViewUvIndex)
        val textViewWindSpeed = view.findViewById<TextView>(R.id.textViewWindSpeed)
        val textViewVisibility = view.findViewById<TextView>(R.id.textViewVisibility)

        val bundleData = arguments?.getParcelable<CurrentlyWeather>("currentlyData")
        bundleData?.apply {
            textViewHumidity.text = humidity.toString()
            textViewUvIndex.text = uvIndex.toString()
            textViewWindSpeed.text = windSpeed.toString()
            textViewVisibility.text = visibility.toString()
        } ?: Toast.makeText(view.context, "Something went wrong", Toast.LENGTH_LONG).show()
        return view
    }

}
