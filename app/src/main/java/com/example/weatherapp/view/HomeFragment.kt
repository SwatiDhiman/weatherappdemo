package com.example.weatherapp.view

import android.net.wifi.hotspot2.pps.HomeSp
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.weatherapp.R
import com.example.weatherapp.model.Weather
import com.example.weatherapp.utils.Constants
import com.example.weatherapp.utils.Constants.Companion.key
import com.example.weatherapp.view.adapter.WeatherAdapter
import com.example.weatherapp.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
    private val adapter: WeatherAdapter by lazy { WeatherAdapter(arrayListOf()) }
    private lateinit var homeViewModel: HomeViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Load data in recycler
        loadData()

        // load data when swipe refresh
        itemsswipetorefresh.setOnRefreshListener {
            // clear adapter
            adapter.updateList(null)
            loadData()
            itemsswipetorefresh.isRefreshing = false
        }
    }

    private fun loadData() {
        // temp weather list
        val weatherList = arrayListOf<Weather>()
        recyclerView.adapter = adapter
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        homeViewModel.init()
        homeViewModel.getData(key, 37.3855, -122.088)
            ?.observe(viewLifecycleOwner, Observer {
                weatherList.add(it)
                adapter.updateList(it)
            })
        homeViewModel.getData(key, 40.7128, -74.0060)
            ?.observe(viewLifecycleOwner, Observer {
                weatherList.add(it)
                adapter.updateList(it)
            })
        homeViewModel.getError()?.observe(viewLifecycleOwner, Observer {
            Toast.makeText(view?.context, it, Toast.LENGTH_SHORT).show()
        })
        adapter.setOnItemClickListener(object : WeatherAdapter.ClickListener {
            override fun onItemClick(v: View, position: Int) {
                val bundle = bundleOf("currentlyData" to weatherList[position].currently)
                v.findNavController().navigate(R.id.action_homeFragment_to_detailsFragment, bundle)
            }
        })
    }

    override fun onStop() {
        super.onStop()
        adapter.updateList(null)
    }
}
