package com.example.weatherapp.view.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weatherapp.R
import com.example.weatherapp.model.Weather
import com.example.weatherapp.utils.DateTimeFormatter
import com.example.weatherapp.utils.WeatherIcons

class WeatherAdapter(private var items: ArrayList<Weather>) :
    RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {
    private var clickListener: ClickListener? = null

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateList(items: Weather?) {
        if (items == null) {
            this.items.clear()
        } else {
            this.items.add(items)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        data.let { holder.bindItems(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(itemView)
    }

    inner class ViewHolder(row: View) : RecyclerView.ViewHolder(row), View.OnClickListener {
        private val textViewTimeOfDay = row.findViewById(R.id.textViewTimeOfDay) as TextView
        private val textViewCityName = row.findViewById(R.id.textViewCityName) as TextView
        private val textViewTemprature = row.findViewById(R.id.textViewTemprature) as TextView
        private val textViewSummary = row.findViewById(R.id.textViewSummary) as TextView
        val imageViewIcon = row.findViewById(R.id.imageViewIcon) as ImageView

        init {
            if (clickListener != null) {
                itemView.setOnClickListener(this)
            }
        }

        @SuppressLint("NewApi")
        fun bindItems(data: Weather) {
            textViewTimeOfDay.text = DateTimeFormatter.timeFormatter(data.currently?.time?.toLong())
            textViewTemprature.text = data.currently?.temperature.toString()
            textViewSummary.text = data.currently?.summary.toString()
            textViewCityName.text = data.timezone
            Glide.with(imageViewIcon.context)
                .load(WeatherIcons.map()[data.currently?.icon])
                .into(imageViewIcon)
        }

        override fun onClick(v: View?) {
            if (v != null) {
                clickListener?.onItemClick(v, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    interface ClickListener {
        fun onItemClick(v: View, position: Int)
    }
}