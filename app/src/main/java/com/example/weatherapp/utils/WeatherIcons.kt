package com.example.weatherapp.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.example.weatherapp.R

object WeatherIcons {
    fun map(): Map<String, Int> {
        val weatherIconMap = HashMap<String, Int>()
        weatherIconMap["clear-day"] = R.drawable.ic_weather_clear_day
        weatherIconMap["clear-night"] = R.drawable.ic_weather_clear_night
        weatherIconMap["rain"] = R.drawable.ic_weather_rain
        weatherIconMap["snow"] = R.drawable.ic_weather_snow
        weatherIconMap["sleet"] = R.drawable.ic_weather_sleet
        weatherIconMap["wind"] = R.drawable.ic_weather_wind
        weatherIconMap["fog"] = R.drawable.ic_weather_fog
        weatherIconMap["cloudy"] =
            R.drawable.ic_weather_cloudy
        weatherIconMap["partly-cloudy-day"] =
            R.drawable.ic_weather_partly_cloudy_day
        weatherIconMap["partly-cloudy-night"] =
            R.drawable.ic_weather_party_cloudy_night
        return weatherIconMap
    }
}