package com.example.weatherapp.utils

import android.icu.text.DateFormat
import android.icu.text.SimpleDateFormat
import android.os.Build
import androidx.annotation.RequiresApi
import java.util.*
import java.util.concurrent.TimeUnit

class DateTimeFormatter {

    companion object {

        @RequiresApi(Build.VERSION_CODES.N)
        fun timeFormatter(timeMillis: Long?): String {

            val formatter: DateFormat = SimpleDateFormat("hh:mm aa")

            val calendar = Calendar.getInstance()
            if (timeMillis != null) {
                calendar.timeInMillis = timeMillis
            }

            return formatter.format(calendar.getTime())
        }
    }
}